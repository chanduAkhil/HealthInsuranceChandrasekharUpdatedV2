package com.emids.healthInsurance;

public class Context {
	
	
	private Premiumstrategy premiumstrategy;
	
	public Context(Premiumstrategy premiumstrategy)
	{
		this.premiumstrategy=premiumstrategy;
	}
	
	public int calculatePremium(int premium) {
		
		return premiumstrategy.changeInPremium(premium);
		
	}

}
